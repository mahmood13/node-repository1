var http = require('http');
var fs = require('fs');
var server = http.createServer(function(req, res) {
    if (req.method == "GET") {
        console.log("request GET come");
        if (req.url == "/") {
            console.log("/");
            fs.readFile("./viues/home.html", function(err, data) {
                if (err) {
                    console.log(err);
                } else {
                    res.writeHead(200, { "Content-Type": "text/html" });
                    res.write(data);
                    res.end();
                }
            });

        } else if (req.url == "/about") {
            console.log("/about");
            res.writeHead(200, { "Content-Type": "text/html" });
            fs.createReadStream("./viues/about.html").pipe(res);


        } else {
            console.log(":((");
            res.end("page not found");

        }
    }
});
server.listen(6504);
console.log("server runing in port 6504");